const express = require('express');
const router = express.Router();
const CharityController = require('../Controllers/CharityController');

router.post('/create-update', CharityController.create_and_update);
router.get('/all', CharityController.find_all);

router.get('/delete/:charityid', CharityController.delete);
router.get('/delete-all', CharityController.delete_all);

router.get('/detail/:charityid', CharityController.find_one);
router.get('/detailname/:charityname', CharityController.find_one_name);

// Array of ID and Names as a output - ALL Data no filter
router.get('/allidnames', CharityController.find_all_idname);

// Filter multiple names 
router.post('/multiplenames', CharityController.find_all_name_filtermany);

// Filter with id and name
router.post('/idandname', CharityController.find_one_idandname_filter);
// Filter with id or name
router.post('/idorname', CharityController.find_one_idorname_dyn_filter);

//Temporary for Testing
//router.get('/email/:charityid', CharityController.find_one_email);
//router.post('/multiplenamesremarksupdate', CharityController.find_all_name_update_all);

module.exports = router;


/* id ,name   as a array */
/* id ,name and address */
/* filter api  -- multiple name or id ... .response in aarray */ 