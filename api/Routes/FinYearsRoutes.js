const express = require('express');
const router = express.Router();
const FinYearsController = require('../Controllers/FinYearsController');

router.post('/create-update', FinYearsController.create_and_update);
router.get('/all', FinYearsController.find_all);

// finyear id input is must and required .. will return total tran amount of that year
router.get('/:finYearId', FinYearsController.find_one);
// finyear id and org id input is must and required .. will return total tran amount of that year for org id  
router.post('/findFinYearCharitySummary', FinYearsController.find_finyear_charity_summary);

// Dynamic ...any one input is also fine... finyear id is input 
router.post('/findMultipleFilter', FinYearsController.find_all_filter);

// Dynamic ...any one input is also fine... finyear id is input using procedure 
router.post('/findFinYear', FinYearsController.find_finyear_summary);


// stored procedure test 
router.post('/testprocedure', FinYearsController.find_finyear_summary);


module.exports = router;


/* id ,name   as a array */
/* id ,name and address */
/* filter api  -- multiple name or id ... .response in aarray */