const express = require('express');
const router = express.Router();
const CharityTransController = require('../Controllers/CharityTransController');

router.post('/create-update', CharityTransController.create_and_update);
router.get('/all', CharityTransController.find_all);
router.get('/delete/:srno', CharityTransController.delete);
router.get('/delete-all', CharityTransController.delete_all);

// Find one Transaction no
router.get('/detail/:srno', CharityTransController.find_one);
 

// list all Transaction no
router.post('/listTrans', CharityTransController.list_all_srno);

// list all Transaction no
//router.post('/listTrans', CharityTransController.list_all_srno);

// reports for org id and fin year id wise
router.post('/findOrgIdFinYearId', CharityTransController.find_all_org_finyear_trans);
// Multiple Transaction no 
router.post('/findMultipleTransNo', CharityTransController.find_all_transno);

// Dynamic ...any one input is also fine... trans no or charity id 
router.post('/findMultipleTransFilter', CharityTransController.find_all_trans_filter);

// Dynamic ...Send Email ... any one input is also fine... trans no or charity id 
router.post('/findandSendEmail', CharityTransController.find_all_trans_sendEmail);

// Dynamic ...any one input is also fine... trans no or charity id .. reports filter many inputs
router.post('/findMultipleReportFilter', CharityTransController.find_all_reports_filter);

// Dynamic ...any one input is also fine... trans no or charity id .. reports filter many inputs
router.post('/findjoindata', CharityTransController.find_join_data_old1);

// Dynamic ...any one input is also fine... trans no or charity id .. reports filter many inputs
router.post('/findjoindata1', CharityTransController.find_join_data_final);

module.exports = router;  