const express = require('express');
const router = express.Router();
const FinLedgerController = require('../Controllers/FinLedgerController');

router.post('/bulk-create-update', FinLedgerController.bulk_create_and_update);
router.post('/create-update', FinLedgerController.bulk_create_and_update);
//router.post('/create-update', FinLedgerController.create_and_update);
router.get('/all', FinLedgerController.find_all);

// Dynamic ...any one input is also fine... finyear id is input 
router.post('/findMultipleLedgerFilter', FinLedgerController.find_all_ledger_filter);

// Dynamic ...any one input is also fine... finyear id is input .. new for calling procedure 
router.post('/findYearFooter', FinLedgerController.find_year_ledger_footer); 

module.exports = router;


/* id ,name   as a array */
/* id ,name and address */
/* filter api  -- multiple name or id ... .response in aarray */ 