const express = require('express');
const router = express.Router();
require('../../configs/authentication');

//require('../../configs/authentication');
const passport = require('passport');

const UserController = require('../Controllers/UserController');
const { body, validationResult } = require("express-validator");

router.post('/create-user', UserController.create_user);

router.get('/all-users', passport.authenticate('jwt', { failWithError: true }),  UserController.get_all_users);
router.post('/userdetails', passport.authenticate('jwt', { failWithError: true }), UserController.get_one_userdetails);

router.get('/delete-user/:userId', UserController.delete_user);
router.post('/login', [
  body("email", "Kindly provide us an appropriate email address").isEmail(),
  body("password", "Kindly provide the Password").not().isEmpty()
], UserController.sign_in);
// router.post('/refresh', UserController.refresh_token);
// router.get('/forgot-password', UserController.forgot_password);
// router.post('/reset-password', UserController.reset_password);


module.exports = router;