const Sequelize = require('sequelize');
const db = require('../../configs/database');

const TransAlarmrecords = db.define('transAlarmrecords', {
    alrID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'alrID',
    },
    smSiteID: {
        type: Sequelize.INTEGER,
        field: 'smSiteID',
    },
    smSiteCode: {
        type: Sequelize.STRING,
        field: 'smSiteCode',
    },
    smSiteName: {
        type: Sequelize.STRING,
        field: 'smSiteName',
    },
    crClusterID: {
        type: Sequelize.INTEGER,
        field: 'crClusterID',
    },
    crName: {
        type: Sequelize.STRING,
        field: 'crName',
    },
    rgRegionID: {
        type: Sequelize.INTEGER,
        field: 'rgRegionID',
    },
    rgRegion: {
        type: Sequelize.STRING,
        field: 'rgRegion',
    },
    znZoneID: {
        type: Sequelize.INTEGER,
        field: 'znZoneID',
    },
    znZone: {
        type: Sequelize.STRING,
        field: 'znZone',
    },
    emEmpID: {
        type: Sequelize.INTEGER,
        field: 'emEmpID',
    },
    emEmployeeID: {
        type: Sequelize.STRING,
        field: 'emEmployeeID',
    },
    emFirstName: {
        type: Sequelize.STRING,
        field: 'emFirstName',
    },
    emContactNo: {
        type: Sequelize.STRING,
        field: 'emContactNo',
    },
    emEmail: {
        type: Sequelize.STRING,
        field: 'emEmail',
    },
    AccID: {
        type: Sequelize.INTEGER,
        field: 'AccID',
    },
    alrPinNumber: {
        type: Sequelize.STRING,
        field: 'alrPinNumber',
    },
    alName: {
        type: Sequelize.STRING,
        field: 'alName',
    },
    alPinCriticality: {
        type: Sequelize.INTEGER,
        field: 'alPinCriticality',
    },
    alrOpenTime: {
        type: Sequelize.INTEGER,
        field: 'alrOpenTime',
    },
    alrCloseTime: {
        type: Sequelize.INTEGER,
        field: 'alrCloseTime',
    },
    alrIsValid: {
        type: Sequelize.INTEGER,
        field: 'alrIsValid',
    },
    alrDemonProcessed: {
        type: Sequelize.INTEGER,
        field: 'alrDemonProcessed',
    },
    alrIsAPINotificationSent: {
        type: Sequelize.INTEGER,
        field: 'alrIsAPINotificationSent',
    },
    alrAPINotificationSentDate: {
        type: Sequelize.INTEGER,
        field: 'alrAPINotificationSentDate',
    },
    alrProcessed: {
        type: Sequelize.INTEGER,
        field: 'alrProcessed',
    },
    alrIsEBDisplay: {
        type: Sequelize.INTEGER,
        field: 'alrIsEBDisplay',
    },
    alrType: {
        type: Sequelize.INTEGER,
        field: 'alrType',
    },
    alrHasFaultCreated: {
        type: Sequelize.INTEGER,
        field: 'alrHasFaultCreated',
    },
    alrFaultRefNumber: {
        type: Sequelize.STRING,
        field: 'alrFaultRefNumber',
    },
    alrFaultStatus: {
        type: Sequelize.INTEGER,
        field: 'alrFaultStatus',
    },
    alrEscalatedLevel: {
        type: Sequelize.INTEGER,
        field: 'alrEscalatedLevel',
    },
    alrIsEscalationClosed: {
        type: Sequelize.INTEGER,
        field: 'alrIsEscalationClosed',
    },
    alrIsSEEscalationClosed: {
        type: Sequelize.INTEGER,
        field: 'alrIsSEEscalationClosed',
    },
    alrAlarmValue: {
        type: Sequelize.FLOAT,
        field: 'alrAlarmValue',
    },
    alrHasIntimated: {
        type: Sequelize.INTEGER,
        field: 'alrHasIntimated',
    },
    alrCreationTime: {
        type: Sequelize.INTEGER,
        field: 'alrCreationTime',
    },
    alrUpdateTime: {
        type: Sequelize.INTEGER,
        field: 'alrUpdateTime',
    },
    alrFaultUpdatedby: {
        type: Sequelize.STRING,
        field: 'alrFaultUpdatedby',
    },
    alrFaultUpdatedDateTime: {
        type: Sequelize.INTEGER,
        field: 'alrFaultUpdatedDateTime',
    },
    alrAcknowledgedBy: {
        type: Sequelize.STRING,
        field: 'alrAcknowledgedBy',
    },
    alrAcknowledgedDateTime: {
        type: Sequelize.INTEGER,
        field: 'alrAcknowledgedDateTime',
    },
    alDesc: {
        type: Sequelize.STRING,
        field: 'alDesc',
    },
    etEquipmentID: {
        type: Sequelize.INTEGER,
        field: 'etEquipmentID',
    },
    etEquipmentCode: {
        type: Sequelize.STRING,
        field: 'etEquipmentCode',
    },
    alrAlmpSyncStatus: {
        type: Sequelize.INTEGER,
        field: 'alrAlmpSyncStatus',
    },
    alrAlmpSyncDate: {
        type: Sequelize.INTEGER,
        field: 'alrAlmpSyncDate',
    },
    alrTTCreationStatus: {
        type: Sequelize.INTEGER,
        field: 'alrTTCreationStatus',
    },
    alrTTNumber: {
        type: Sequelize.INTEGER,
        field: 'alrTTNumber',
    },
    alrTTEscalatedLevel: {
        type: Sequelize.INTEGER,
        field: 'alrTTEscalatedLevel',
    },
    alrIsTTEscalationStatus: {
        type: Sequelize.INTEGER,
        field: 'alrIsTTEscalationStatus',
    },
    alrIsTTCreationReq: {
        type: Sequelize.INTEGER,
        field: 'alrIsTTCreationReq',
    },
    alrSymptAlarmEscalationLevel: {
        type: Sequelize.INTEGER,
        field: 'alrSymptAlarmEscalationLevel',
    },
    alrIsSymptAlarmEscalationStatus: {
        type: Sequelize.INTEGER,
        field: 'alrIsSymptAlarmEscalationStatus',
    },
    alrGCMSyncStatus: {
        type: Sequelize.INTEGER,
        field: 'alrGCMSyncStatus',
    },
    alrGCMSyncTime: {
        type: Sequelize.INTEGER,
        field: 'alrGCMSyncTime',
    },
    alrTemperature: {
        type: Sequelize.FLOAT,
        field: 'alrTemperature',
    },
    alrstatus: {
        type: Sequelize.INTEGER,
        field: 'alrstatus',
    },
    ttStatus: {
        type: Sequelize.INTEGER,
        field: 'ttStatus',
    },
    ttlstescalatedmailid: {
        type: Sequelize.STRING,
        field: 'ttlstescalatedmailid',
    },
    ttClosedMailSent: {
        type: Sequelize.INTEGER,
        field: 'ttClosedMailSent',
    },
    smSitetypeid: {
        type: Sequelize.INTEGER,
        field: 'smSitetypeid',
    },
    hpdate: {
        type: Sequelize.DATE,
        field: 'hpdate',
    }
}, { tableName: 'trans_alarmrecords' }, { timestamps: false }, { underscored: true });



module.exports = TransAlarmrecords;

