const Sequelize = require('sequelize');
const db = require('../../configs/database');
const moment = require('moment');
const Finyears = require('./Finyears');
const Charity = require('./Charity');

const CharityTransactions = db.define('CharityTransactions', {
    srno: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'srno',
    },
    charityid: {
        type: Sequelize.INTEGER,
        field: 'charityid',
        references: {
            model: 'charity',
            key: 'charityid'
        }
    },
    orgid: {
        type: Sequelize.INTEGER,
        field: 'orgid',
        references: {
            model: 'org',
            key: 'charityid'
        }
    },
    finyearid: {
        type: Sequelize.INTEGER,
        field: 'finyearid',
        references: {
            model: 'finyears',
            key: 'finyearid'
        }
    },
    suggested_amount: {
        type: Sequelize.FLOAT,
        field: 'suggested_amount'
    },
    amount: {
        type: Sequelize.FLOAT,
        field: 'amount'
    },
    amount_in_words: {
        type: Sequelize.STRING,
        field: 'amount_in_words'
    },
    modeofpay: {
        type: Sequelize.STRING,
        field: 'modeofpay'
    },
    paidvia_cc: {
        type: Sequelize.STRING,
        field: 'paidvia_cc'
    },
    paid_from: {
        type: Sequelize.STRING,
        field: 'paid_from'
    },
    irs_ok: {
        type: Sequelize.STRING,
        field: 'irs_ok'
    },
    receipt: {
        type: Sequelize.STRING,
        field: 'receipt'
    },
    receipt_quicken: {
        type: Sequelize.STRING,
        field: 'receipt_quicken'
    },
    direction_of: {
        type: Sequelize.STRING,
        field: 'direction_of'
    },
    wording_direction: {
        type: Sequelize.STRING,
        field: 'wording_direction'
    },
    courties_copies: {
        type: Sequelize.STRING,
        field: 'courties_copies'
    },
    date_of_tran: {
        type: Sequelize.DATE,
        field: 'date_of_tran',
        defaultValue: moment().format('YYYY-MM-DD hh:mm:ss'),
    },
    transmite_date: {
        type: Sequelize.DATE,
        field: 'transmite_date',
        defaultValue: moment().format('YYYY-MM-DD hh:mm:ss'),
    },
    check_date: {
        type: Sequelize.DATE,
        field: 'check_date',
        defaultValue: moment().format('YYYY-MM-DD hh:mm:ss'),
    },
    email_sent: {
        type: Sequelize.STRING,
        field: 'email_sent',
        validate: {

        }
    },
    remarks: {
        type: Sequelize.STRING,
        field: 'remarks'
    },
    createdate: {
        type: Sequelize.DATE,
        field: 'createdate'
    },
    createdby: {
        type: Sequelize.STRING,
        field: 'createdby'
    }
}, { tableName: 'charity_transactions' }, { timestamps: false }, { underscored: true });

Finyears.hasOne(CharityTransactions, { foreignKey: 'finyearid' });
CharityTransactions.hasOne(Charity, { foreignKey: 'charityid' });


module.exports = CharityTransactions;
