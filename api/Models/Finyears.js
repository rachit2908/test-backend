const Sequelize = require('sequelize');
const db = require('../../configs/database');

const Finyears = db.define('finyears', {
    finyearid: {
        type: Sequelize.STRING,
        primaryKey: true,
        autoIncrement: true,
        field: 'finyearid',
    },
    finyeardesc: {
        type: Sequelize.STRING,
        field: 'finyeardesc',
    },
    start_date: {
        type: Sequelize.DATE,
        field: 'start_date',
    },
    end_date: {
        type: Sequelize.DATE,
        field: 'end_date',
    },
    target_amount: {
        type: Sequelize.STRING,
        field: 'target_amount',
    },
}, { tableName: 'finyears' }, { timestamps: false }, { underscored: true });


module.exports = Finyears;