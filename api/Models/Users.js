const Sequelize = require('sequelize');
const db = require('../../configs/database');
const moment = require('moment');

const Users = db.define('users', {
    userId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'userId',
    },
    user_name: {
        type: Sequelize.STRING,
        field: 'user_name',
    },
    user_type: {
        type: Sequelize.INTEGER,
        field: 'user_type',
    },
    email: {
        type: Sequelize.STRING,
        field: 'email',
    },
    phone_no: {
        type: Sequelize.STRING,
        field: 'phone_no',
    },
    password: {
        type: Sequelize.STRING,
        field: 'password',
    },
    status: {
        type: Sequelize.INTEGER,
        field: 'status',
        defaultValue: 1
    },
    created_datetime: {
        type: Sequelize.DATE,
        field: 'created_datetime',
        defaultValue: moment.utc(new Date()).format('YYYY-MM-DD hh:mm:ss'),
        validate: {
            notEmpty: false
        }
    },
    updated_datetime: {
        type: Sequelize.DATE,
        field: 'updated_datetime',
        defaultValue: moment.utc(new Date()).format('YYYY-MM-DD hh:mm:ss'),
        validate: {
            notEmpty: false
        }
    }
}, { tableName: 'users' }, { timestamps: false }, { underscored: true });



module.exports = Users;
