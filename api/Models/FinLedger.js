const Sequelize = require('sequelize');
const db = require('../../configs/database');
const moment = require('moment');

const Finledger = db.define('finledger', {    
    // srno: {
    //     type: Sequelize.INTEGER,
    //     //primaryKey: true,
    //     //autoIncrement: true,
    //     field: 'seqno',
    //     defaultValue: 1
    // },    
    seqno: {
        type: Sequelize.INTEGER,
        field: 'seqno',
        defaultValue: 9999
    },
    description: {
        type: Sequelize.STRING,
        field: 'description',
    },
    amount: {
        type: Sequelize.FLOAT  ,
        field: 'amount',
    },
    remarks: {
        type: Sequelize.STRING,
        field: 'remarks',
    },    
    finyearid: {
        type: Sequelize.STRING,
        field: 'finyearid',
    },
}, { tableName: 'finledger' }, { timestamps: false }, { underscored: true });

//if primary key is not maintained it will take id as default column no 
Finledger.removeAttribute("id")
module.exports = Finledger;