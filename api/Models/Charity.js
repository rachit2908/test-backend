const Sequelize = require('sequelize');
const db = require('../../configs/database');
const moment = require('moment');

const Charity = db.define('charity', {
    charityid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'charityid',
    },
    charityname: {
        type: Sequelize.STRING,
        field: 'charityname',
        // references: {
        //     model: 'user',
        //     key: 'user_rid'
        // }
    },
    ein: {
        type: Sequelize.STRING,
        field: 'ein',
    },
    contactname: {
        type: Sequelize.STRING,
        field: 'contactname'
    },
    dearwhom: {
        type: Sequelize.STRING,
        field: 'dearwhom'
    },
    address1: {
        type: Sequelize.STRING,
        field: 'address1'
    },
    address2: {
        type: Sequelize.STRING,
        field: 'address2'
    },
    city: {
        type: Sequelize.STRING,
        field: 'city'
    },
    state: {
        type: Sequelize.STRING,
        field: 'state',
        defaultValue: ''
    },
    postalcode: {
        type: Sequelize.STRING,
        field: 'postalcode'
    },
    emailid1: {
        type: Sequelize.STRING,
        field: 'emailid1'
    },
    emailid2: {
        type: Sequelize.STRING,
        field: 'emailid2'
    },
    phone1: {
        type: Sequelize.STRING,
        field: 'phone1'
    },
    phone2: {
        type: Sequelize.STRING,
        field: 'phone2'
    },
    url: {
        type: Sequelize.STRING,
        field: 'url'
    },
    active: {
        type: Sequelize.STRING,
        field: 'active'
    },
    remarks: {
        type: Sequelize.STRING,
        field: 'remarks'
    },
    suggested_amount: {
        type: Sequelize.FLOAT,
        field: 'suggested_amount',
        defaultValue: 0
    },
    amount_sent: {
        type: Sequelize.FLOAT,
        field: 'amount_sent',
        defaultValue: 0
    },
    createdate: {
        type: Sequelize.DATE,
        field: 'createdate',
        defaultValue: moment().format('YYYY-MM-DD hh:mm:ss'),
        validate: {
            notEmpty: false
        }
    },
    createdby: {
        type: Sequelize.STRING,
        field: 'createdby'
    }
}, { tableName: 'charity' }, { timestamps: false }, { underscored: true });

module.exports = Charity;