const Sequelize = require('sequelize');
const db = require('../../configs/database');

const Org = db.define('org', {
    orgid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'orgid',
    },
    orgname: {
        type: Sequelize.STRING,
        field: 'orgname',
    },
}, { tableName: 'org' }, { timestamps: false }, { underscored: true });



module.exports = Org;