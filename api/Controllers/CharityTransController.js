const db = require("../../configs/database");
const CharityTrans = require("../Models/CharityTransactions");
const Charity = require("../Models/Charity");
const Op = require("sequelize").Op;
const Sequelize = require("sequelize");
const responseHelper = require("../../helpers/ApiResponseHelper");
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

var log = function (inst) {
  console.dir(inst.get());
};

exports.create_and_update = (req, res, next) => {
  var body = req.body;
  console.log("test create transaction ");
  if (body.srno && body.srno > 0) {
    CharityTrans.update(body, {
      where: { srno: body.srno },
    })
      .then((CharityTrans) => {
        responseHelper.sendSuccessResponse(
          res,
          "CharityTrans updated successfully",
          null
        );
      })
      .catch((err) => {
        responseHelper.sendFailResponse(res, "failed to update CharityTrans");
      });
  } else {
    console.log("create transaction ");
    CharityTrans.create(body)
      .then((CharityTrans) => {
        return responseHelper.sendSuccessResponse(
          res,
          "CharityTrans created successfully",
          CharityTrans
        );
      })
      .catch((err) => {
        return responseHelper.sendFailResponse(
          res,
          "failed to create CharityTrans"
        );
      });
  }
};

exports.find_all = (req, res, next) => {
  CharityTrans.findAll()
    .then((charities) => {
      return responseHelper.sendResponse(
        res,
        200,
        0,
        "charities transactions data",
        charities
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(
        res,
        "failed to get charities transactions"
      );
    });
};

exports.delete = (req, res, next) => {
  var srno = req.params.srno;
  console.log(srno);
  CharityTrans.destroy({
    where: { srno: srno },
  })
    .then((charities) => {
      return responseHelper.sendSuccessResponse(
        res,
        "CharityTrans deleted successfully",
        charities
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(
        res,
        "failed to delete CharityTrans"
      );
    });
};

exports.find_one = (req, res, next) => {
  var srno = req.params.srno;
  CharityTrans.findByPk(srno)
    .then((CharityTrans) => {
      return responseHelper.sendSuccessResponse(
        res,
        "CharityTrans details",
        CharityTrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(res, "failed to get CharityTrans");
    });
};
exports.delete_all = (req, res, next) => {
  CharityTrans.truncate()
    .then((CharityTrans) => {
      return responseHelper.sendSuccessResponse(
        res,
        "CharityTrans table truncated successfully",
        CharityTrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(
        res,
        "failed to truncated CharityTrans table"
      );
    });
};

exports.email_sent_update = (req, res, next) => {
  var body = req.body;
  if (body.srno && body.srno > 0) {
    CharityTrans.update(body, {
      where: { email_sent: "Y" },
    })
      .then((CharityTrans) => {
        responseHelper.sendSuccessResponse(
          res,
          "CharityTrans Emails Sent and Updated successfully",
          null
        );
      })
      .catch((err) => {
        responseHelper.sendFailResponse(
          res,
          "failed to sent Email and update CharityTrans"
        );
      });
  }
};

// Filter for Org Id and Financial Year Records
// only 1 orgid and 1 fin year id
exports.find_all_org_finyear_trans = (req, res, next) => {
  var orgidlist = req.body.orgidlist;
  var finyearidlist = req.body.finyearidlist;
  CharityTrans.findAll({
    where: {
      orgid: orgidlist,
      finyearid: finyearidlist,
    },
  })
    .then((charityTrans) => {
      return responseHelper.sendSuccessResponse(
        res,
        "charity trans details",
        charityTrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(
        res,
        "failed to get trans charity"
      );
    });
};

// Dynamic where clause depend upon the inputs
// as of now only 1 id or 1 name is supported in input
// multiple supported ..code fixed
exports.find_all_trans_filter = (req, res, next) => {
  //var charityname = req.params.charityname;
  //var charityid = req.params.charityid;

  console.log("inside function find_one_idandname_filter");
  var transnolist = req.body.transnolist;
  var charityidlist = req.body.charityidlist;
  var orgid = req.body.orgid;
  var finyearid = req.body.finyearid;

  var var_email_sent = req.body.email_sent;
  var var_receipt_quicken = req.body.receipt_quicken;
  console.log("Get the trans no length   " + transnolist.length);
  console.log("Get the trans no list   " + transnolist);
  console.log("Get the charity id list " + charityidlist);
  console.log("Get the orgidid id      " + orgid);
  console.log("Get the finyearid id    " + finyearid);

  //const { charityname } = req.body;
  // Charity.findOne({ charityname }, function (err, charity) {
  //   if (!charity) {
  //     return res.status(400).json({
  //       err: "charity name does not exist",
  //     });
  //   }

  // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
  //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
  // }).catch((err) => {
  //     return responseHelper.sendFailResponse(res, 'failed to get charity');
  // })

  //console.dir(transnolist);
  //console.dir(charityidlist);

  let queryObject = {};
  queryObject.where = {};

  //if(charityidlist && transnolist )
  if (charityidlist.length > 0 && transnolist.length > 0) {
    // && var_email_sent.length >0  && var_receipt_quicken.length >0
    console.log("id and trans no ");
    queryObject.where.charityid = { [Op.in]: charityidlist };
    queryObject.where.srno = { [Op.in]: transnolist };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
    //queryObject.where.email_sent = { [Op.in]: var_email_sent  };
    //queryObject.where.receipt_quicken = { [Op.in]: var_receipt_quicken  };
  } else if (transnolist.length > 0) {
    console.log("only trans no ");
    queryObject.where.srno = { [Op.in]: transnolist };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
  } else if (charityidlist.length > 0) {
    console.log("only charity id ");
    queryObject.where.charityid = { [Op.in]: charityidlist };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
  } else {
    console.log("only fin year and org id ");
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
  }

  CharityTrans.findAll(queryObject)
    .then((charityTrans) => {
      return responseHelper.sendSuccessResponse(
        res,
        "charity details",
        charityTrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(res, "failed to get charity");
    });

  // Charity.findAll(
  // {
  //     where:{
  //         charityname:  charitynamelist,
  //         charityid:    parseInt(charityidlist)
  //           }
  // }  ).then((charity) => {
  //  return responseHelper.sendSuccessResponse(res, 'charity details', charity)
  // }).catch((err) => {
  //  return responseHelper.sendFailResponse(res, 'failed to get charity');
  // })

  // var body = req.body;
  // if (body.charityid && body.charityid > 0) {
  //     var charitynamelist =    req.body.charitynamelist;
  //     var charityidlist   =    req.body.charityidlist;
};

//Dynamic where clause depend upon the inputs
// as of now only 1 id or 1 name is supported in input
exports.find_all_trans_sendEmail = (req, res, next) => {
  //var charityname = req.params.charityname;
  //var charityid = req.params.charityid;

  console.log("inside function find_one_idandname_filter");
  var transnolist = req.body.transnolist;
  var charityidlist = req.body.charityidlist;
  //console.log("Get the   trans no list - len  " + transnolist.length);
  //console.log("Get the charity id list - len " + charityidlist.length);

  var orgid = req.body.orgid;
  var finyearid = req.body.finyearid;

  console.log("Get the   trans no list   " + transnolist);
  console.log("Get the charity id list " + charityidlist);

  let queryObject = {};
  queryObject.where = {};

  if (charityidlist.length > 0 && transnolist.length > 0) {
    console.log("id and trans no ");
    queryObject.where.charityid = { [Op.in]: parseInt(charityidlist) };
    queryObject.where.srno = { [Op.in]: transnolist };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
  } else if (transnolist.length > 0) {
    console.log("only trans no ");
    queryObject.where.srno = { [Op.in]: transnolist };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
  } else if (charityidlist.length > 0) {
    console.log("only charity id ");
    queryObject.where.charityid = { [Op.in]: parseInt(charityidlist) };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    queryObject.where.orgid = { [Op.in]: orgid };
  }

  console.log("Before Email Function 0000 ");

  CharityTrans.findAll(queryObject)
    .then(async (charityTrans) => {
      for (let i = 0; i < charityTrans.length; i++) {
        console.log(charityTrans[i].dataValues.charityid);
        console.log(charityTrans[i].dataValues.srno);

        console.log("Before Email Function 0 ");

        if (charityTrans[i].dataValues.srno > 0) {
          //console.log("addn(1,2)  beffore calling email ");
          //let retstr=emailtocharity(charity[i].dataValues.charityname ) ;
          //emailtocharity(charity[i].dataValues.charityname,function(retstr) ) ;
          //var retstr = await emailtocharity(charityTrans[i].dataValues.srno);
          console.log("Before Email Function  ");
          var retstr = await emailtocharity(charityTrans[i]);
          console.log("Email Function returns " + retstr);

          CharityTrans.update(
            {
              email_sent: "Y",
              createdate: Date.now(),
            },
            {
              where: { srno: charityTrans[i].dataValues.srno },
            }
          ).then((count) => {
            console.log("Rows updated " + count);
          });
        }
      }
      return responseHelper.sendSuccessResponse(
        res,
        "charity details",
        charityTrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(res, "failed to get charity");
    });
};

exports.find_all_transno = (req, res, next) => {
  var transnolist = req.body.transnolist;

  //const { charityname } = req.body;
  // Charity.findOne({ charityname }, function (err, charity) {
  //   if (!charity) {
  //     return res.status(400).json({
  //       err: "charity name does not exist",
  //     });
  //   }

  // charityname: { [Op.or]: [charityIdArr])

  CharityTrans.findAll({
    where: { srno: { [Op.or]: [transnolist] } },
  })
    .then((charityTrans) => {
      //console.log(charity);
      // for (let i = 0; i < charity.length; i++)
      // {
      //     console.log(charity[i].dataValues.charityid  );
      //     console.log(charity[i].dataValues.charityname  );
      // }
      return responseHelper.sendSuccessResponse(
        res,
        "charity transaction details",
        charityTrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(
        res,
        "failed to get charity transactions"
      );
    });
};

// Function Email Pass the Name as a Paramater
async function emailtocharity(object1) {
  var isValid = "Y";
  var retstr = "0";

  //https://www.twilio.com/blog/sending-bulk-emails-3-ways-sendgrid-nodejs#:~:text=The%20most%20straightforward%20way%20to,%2Fmail')%3B%20sgMail.
  //var str1 = "Hello ";
  //var str2 = "<p>Get your <b>Tesla</b> today!</p>";
  //var str3 = "<p>How are you doing?</p>";
  //var res = str1 + str2 + str3;
  //var str4 = str1.concat(str2);
  //var  = str3.concat(res);
  console.log("Email Function ");
  console.log("sr no  - " + object1.dataValues.srno);
  console.log("amount - " + object1.dataValues.amount);

  dataret = await find_charity_and_trans_details_join_data_return(
    object1.dataValues.srno
  );
  console.log("amount from return function      - ", dataret[0].amount);
  console.log("charityname from return function - ", dataret[0].charityname);
  //dataret = await find_charity_and_trans_details_join_data_return(object1.dataValues.srno);
  //console.log("amount from return function - ", dataret.TextRow.amount);
  //console.log("amount from return function - " , JSON.stringify(dataret)    );
  //console.log("amount from return function - ");
  // for (const [key, value] of Object.entries(dataret)) {
  //   console.log("key ", key,  "value " , value);
  // }

  var datetime = new Date();
  console.log(datetime);
  var moment = require("moment");
  //var created = moment().format('YYYY-MM-DD hh:mm:ss')
  var created = moment().format("YYYY-MM-DD");
  console.log(created);

  var var1 =
    ' <div style="text-align: center; float: center;font-weight: bold;"> The ' +
    dataret[0].orgname +
    " <br/>2300 N Street, NW, Suite 300-RLK<br/>Washington, DC 20037</div> <br/><br/>";
  var vardt =
    ' <div style="text-align: center; float: center;">' + created + "<br/>";
  var var2 =
    ' <div style="text-align: left;float: left;">' +
    dataret[0].charityname +
    "<br/>Attention: " +
    dataret[0].contactname +
    "<br/>" +
    dataret[0].address1 +
    "<br>" +
    dataret[0].address2 +
    " <br>" +
    dataret[0].city +
    ", " +
    dataret[0].state +
    ", " +
    dataret[0].postalcode +
    " </div> <br><br><br><br><br><br>";
  var var3 =
    ' <div style="font-weight: bold;padding-top: 20px;padding-left: 70px;">Re: Charitable Gift Our Records Show You Listed on the IRS Site as ' +
    dataret[0].charityname +
    " With EIN of: " +
    dataret[0].ein +
    "</div><br>";
  var var4 =
    ' <p style="text-align: left;float: left;"> Dear ' +
    dataret[0].contactname +
    " :</p> <br>";
  var var5 =
    ' <p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I am pleased to advise you that we have processed a credit card distribution in the amount of ' +
    dataret[0].amount_in_words +
    " (" +
    dataret[0].amount +
    ")  at the direction of Todd E. Harper, the Foundations President, representing a charitable gift to your worthy organization for whatever purposes your organization deems proper.</p>";
  var var6 =
    ' <p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please send us a receipt that is required by the Internal Revenue Service, in keeping with federal and state regulations regarding charitable donations. Enclosed please find a format you are welcome to use. We ask that you kindly deliver to us that receipt by email or regular mail within two (2) weeks.</p>';
  var var7 =
    ' <p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are kindly reminded that we would prefer not to be added to any solicitation lists (although announcements of special events are welcome).</p><br>';
  var var8 =
    ' <div style="float: right;padding-top: 30px;"><p>Sincerely,<p> <br/>' +
    dataret[0].orgname +
    "<br/><br/><br/></div>";
  var var9 =
    ' <div style="float: left;padding-top: 30px;"><br/><p> By: Roy L. Kaufmann<br>Director RLK/dmp Enc.<br>cc:«CC» -«Form_Name»</p><br/> <br/><br/></div>';
  var var10 = " <div><br></div><div><br></div><div><br></div>";

  var var11 =
    ' <div style="text-align: center; float: center;font-family: Arial, Helvetica, sans-serif;"><h3>' +
    dataret[0].charityname +
    "</h3>" +
    dataret[0].address1 +
    "<br>" +
    dataret[0].address2 +
    "<br> EIN: " +
    dataret[0].ein +
    "<br/></div> <br/>";
  var var22 =
    ' <div style="text-align: center; float: center;font-family: Arial, Helvetica, sans-serif;"><h2>Receipt</h2><br/>';
  var var33 = " <div><br></div>";
  var var44 =
    ' <div style="text-align: left; float: left; font-family: Arial, Helvetica, sans-serif; ">Received from: The ' +
    dataret[0].orgname +
    " </div><br/>";
  var var55 =
    ' <div style="text-align: left; float: left; font-family: Arial, Helvetica, sans-serif;">Date: see below </div> <br/>';
  var var66 =
    ' <div style="text-align: left; float: left; font-family: Arial, Helvetica, sans-serif;" > Amount Received: ' +
    dataret[0].amount_in_words +
    " (" +
    dataret[0].amount +
    ") </div> <br><br><hr> ";
  var var77 =
    ' <p style="text-align: justify;font-style: italic;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Gift is tax-deductible to the fullest extent of the law. No goods or services, as defined by the Internal Revenue Service, have been provided as a consideration for this gift, in keeping with federal and state regulations regarding charitable donations</p>';
  var var88 =
    ' <div style="float: right; text-align: left"><p>Authorized Signature <p><br/>Printed Name:_______________<br/> Title:_______________________<br/>Date:______________________<br/><br/><br/><br/><br/><br/> <br/><br/></div><br/><br/>';
  var var99 =
    " <div>&nbsp;<br></div><div>&nbsp;<br></div><div>&nbsp;<br></div><div>&nbsp;<br></div><div>&nbsp;<br></div>";
  var var100 =
    ' <div style="text-align: left; float: left; "><br/><p> Mail Receipt to:<br>' +
    dataret[0].orgname +
    "<br>2300 N Street, NW, Suite 300-RLK<br>Washington, D.C. 20037</p><br/> <br/><br/></div>";

  var final_text1 =
    var1 +
    vardt +
    var2 +
    var3 +
    var4 +
    var5 +
    var6 +
    var7 +
    var8 +
    var9 +
    var10 +
    var10 +
    var10 +
    var10;
  var final_text2 =
    var33 +
    var11 +
    var22 +
    var33 +
    var44 +
    var33 +
    var55 +
    var33 +
    var66 +
    var77 +
    var88 +
    var99 +
    var100;
  var final_text = final_text1 + final_text2;

  var toemail_2 = dataret[0].emailid2;
  var toemail_1 = dataret[0].emailid1;
  var toemail = dataret[0].emailid1 + "," + dataret[0].emailid2;

  var toemail = dataret[0].emailid1;

  console.log("to Email id's");
  console.log(toemail);
  //console.log(dataret[0].emailid2);
  const msg = {
    //to: ['haiharry26@gmail.com,harish260178@gmail.com'], // Change to your recipient
    to: [toemail], // Change to your recipient
    cc: ["harishbogawat@gmail.com"], // Change to your recipient
    from: "harish.kumar@skillnetinc.com", // Change to your verified sender
    subject: "Charity Donation - Email Test",
    //text: 'Hello. This is a notification from the Foundation. Attached Please find Correspondence that may need your attention. Thank you. Roy Kaufmann. Director. RKauf,amm@Jackscamp.com',
    //html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    //html: '<h1>Have the most fun you can in a car!</h1><p>Get your <b>Tesla</b> today!</p>'
    html: final_text,

    // attachments: [
    //   { // Use a URL as an attachment
    //     filename: 'your-testla.png',
    //     path: 'https://media.gettyimages.com/photos/view-of-tesla-model-s-in-barcelona-spain-on-september-10-2018-picture-id1032050330?s=2048x2048'
    // }
    // ]
  };
  return sgMail
    .send(msg)
    .then(() => {
      console.log("Email sent");
      return 1;
    })
    .catch((error) => {
      console.error(error);
      return 0;
    });

  // var nodemailer = require('nodemailer');

  // console.log('Inside Email Function for  user  ' + strname);
  // //send email code
  // var transporter = nodemailer.createTransport({
  //   service: 'gmail',
  //   auth: {
  //     user: 'skillnetincmail@gmail.com',
  //     pass: 'skill@1234'
  //   }
  // });

  // var mailOptions = {
  //   from: 'skillnetincmail@gmail.com',
  //   to: 'harishbogawat@gmail.com',
  //   subject: 'Sending Email using Node.js for id ' + strname,
  //   //text: 'That was easy!'
  //   html: '<h1>Welcome</h1><p>That was easy!</p>'

  // };

  // return await transporter.sendMail(mailOptions).then(info => {
  //   console.log('Email sent: ' + info.response);
  //   retstr = "1";
  //   return retstr;
  // }).catch(err => {
  //   retstr = "0";
  //   return retstr;
  // });

  // function (error,) {
  //   if (error) {
  //     console.log(error);

  //     //callback(retstr);
  //   } else {

  //     //callback(retstr) ;
  //   }
  // });
  //send email code is done

  // if (isValid) {
  //     //next();
  //     return;
  // } else {
  //     //res.send("please try again");
  //     return;
  // }
}

// Function Email Pass the Name as a Paramater
function emailtocharity_mailjet(strname) {
  var isValid = "Y";
  var retstr = "0";
  console.log("Inside Email Function for  mailjet - user  " + strname);

  const mailjet = require("node-mailjet").connect(
    "700812e8024314adad98321596d51452",
    "3d8b78d4034519f4137473c21915c306"
  );
  const request = mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "harishbogawat@gmail.com",
          Name: "Harish",
        },
        To: [
          {
            Email: "harishbogawat@gmail.com",
            Name: "Harish",
          },
        ],
        Subject: "Greetings from Mailjet.",
        TextPart: "My first Mailjet email",
        HTMLPart:
          "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
        CustomID: "AppGettingStartedTest",
      },
    ],
  });
  request
    .then((result) => {
      console.log(result.body);
    })
    .catch((err) => {
      console.log("Error in sending email ");
      console.log(err.statusCode);
    });
}

// Filter for Org Id and Financial Year Records
// only 1 orgid and 1 fin year id
exports.list_all_srno = (req, res, next) => {
  var orgidlist = req.body.orgid;
  var finyearidlist = req.body.finyearid;
  CharityTrans.findAll({
    attributes: ["srno"],
    where: {
      orgid: orgidlist,
      finyearid: finyearidlist,
    },
  })
    .then((charityTrans) => {
      // Force a single returned object into an array.
      console.log("Got the data 1" + JSON.stringify(charityTrans));
      data = [].concat(charityTrans);
      console.log("Got the data 2" + JSON.stringify(data));
      //res.send(data);  // This should maybe be res.json instead...

      return responseHelper.sendSuccessResponse(
        res,
        "charity trans details",
        data
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(
        res,
        "failed to get trans charity"
      );
    });
};

// Dynamic where clause depend upon the inputs
// as of now only 1 id or 1 name is supported in input
// multiple supported ..code fixed
exports.find_all_reports_filter = (req, res, next) => {
  //var charityname = req.params.charityname;
  //var charityid = req.params.charityid;

  console.log("inside function find_all_reports_filter");
  var transnolist = req.body.transnolist;
  var charityidlist = req.body.charityidlist;
  var orgid = req.body.orgid;
  var finyearid = req.body.finyearid;
  var receipt = req.body.receipt;
  var receipt_quicken = req.body.receipt_quicken;
  var irs = req.body.irs;
  var email_sent = req.body.email_sent;

  console.log("Get the trans no        " + transnolist);
  console.log("Get the trans no list   " + transnolist);
  console.log("Get the charity id list " + charityidlist);
  console.log("Get the orgidid id      " + orgid);
  console.log("Get the receipt         " + receipt);
  console.log("Get the receipt_quicken " + receipt_quicken);
  console.log("Get the irs             " + irs);
  console.log("Get the email_sent      " + email_sent);

  //const { charityname } = req.body;
  // Charity.findOne({ charityname }, function (err, charity) {
  //   if (!charity) {
  //     return res.status(400).json({
  //       err: "charity name does not exist",
  //     });
  //   }

  // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
  //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
  // }).catch((err) => {
  //     return responseHelper.sendFailResponse(res, 'failed to get charity');
  // })

  //console.dir(transnolist);
  //console.dir(charityidlist);

  // //if(charityidlist && transnolist )
  // if (charityidlist.length > 0 && transnolist.length > 0 ) {
  //   // && var_email_sent.length >0  && var_receipt_quicken.length >0
  //   console.log("id and trans no ");
  //   queryObject.where.charityid = { [Op.in]: charityidlist };
  //   queryObject.where.srno = { [Op.in]: transnolist };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.orgid = { [Op.in]: orgid };
  //   //queryObject.where.email_sent = { [Op.in]: var_email_sent  };
  //   //queryObject.where.receipt_quicken = { [Op.in]: var_receipt_quicken  };
  // } else if (transnolist.length > 0) {
  //   console.log("only trans no ");
  //   queryObject.where.orgid     = { [Op.in]: orgid };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.srno      = { [Op.in]: transnolist };
  // } else if (charityidlist.length > 0) {
  //   console.log("only charity id ");
  //   queryObject.where.orgid     = { [Op.in]: orgid };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.charityid = { [Op.in]: charityidlist };
  // } else if (receipt.length > 0) {
  //   console.log("only receipt id ");
  //   queryObject.where.orgid     = { [Op.in]: orgid };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.receipt   = { [Op.in]: receipt };
  // } else if (receipt_quicken.length > 0) {
  //   console.log("only receipt_quicken id ");
  //   queryObject.where.orgid     = { [Op.in]: orgid };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.receipt_quicken = { [Op.in]: receipt_quicken };
  // } else if (receipt_quicken.length > 0) {
  //   console.log("only irs id ");
  //   queryObject.where.orgid     = { [Op.in]: orgid };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.irs_ok = { [Op.in]: irs };
  // } else if (email_sent.length > 0) {
  //   console.log("only email_sent ");
  //   queryObject.where.orgid     = { [Op.in]: orgid };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.email_sent = { [Op.in]: email_sent };
  // }  else {
  //   console.log("only fin year and org id ");
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.orgid = { [Op.in]: orgid };
  // }

  let queryObject = {};
  queryObject.where = {};

  queryObject.where.finyearid = { [Op.in]: finyearid };
  queryObject.where.orgid = { [Op.in]: orgid };

  if (charityidlist.length > 0) {
    console.log(" ************charity id ");
    queryObject.where.charityid = { [Op.in]: charityidlist };
  }

  if (transnolist.length > 0) {
    console.log(" ************trans no not null ");
    queryObject.where.srno = { [Op.in]: transnolist };
  }

  if (email_sent.length > 0) {
    console.log(" ************email sent is not  null ");
    queryObject.where.email_sent = { [Op.in]: email_sent };
  }

  if (receipt_quicken.length > 0) {
    console.log(" ************receipt_quicken  is  not null ");
    queryObject.where.receipt_quicken = { [Op.in]: receipt_quicken };
  }

  if (irs.length > 0) {
    console.log(" ************irs  is  not null ");
    queryObject.where.irs_ok = { [Op.in]: irs };
  }

  CharityTrans.findAll(queryObject)
    .then((charitytrans) => {
      console.log(" total records - " + charitytrans.count);
      return responseHelper.sendSuccessResponse(
        res,
        "charity details",
        charitytrans
      );
    })
    .catch((err) => {
      return responseHelper.sendFailResponse(res, "failed to get charity");
    });

  // Charity.findAll(
  // {
  //     where:{
  //         charityname:  charitynamelist,
  //         charityid:    parseInt(charityidlist)
  //           }
  // }  ).then((charity) => {
  //  return responseHelper.sendSuccessResponse(res, 'charity details', charity)
  // }).catch((err) => {
  //  return responseHelper.sendFailResponse(res, 'failed to get charity');
  // })

  // var body = req.body;
  // if (body.charityid && body.charityid > 0) {
  //     var charitynamelist =    req.body.charitynamelist;
  //     var charityidlist   =    req.body.charityidlist;
};

// join working using procedure to return charity and charity transactions
// this function will be called by email to get the details of the object
async function find_charity_and_trans_details_join_data_return(tempsrno) {
  //console.log("inside function find_finyear_summary");
  //var psrno = req.body.inputcharityid;
  var psrno = tempsrno;
  console.log("Get the tran no/srno    " + tempsrno);
  console.log("Get the tran no/srno    " + psrno);
  // db.query("call finYearTarget(?)", [finyearid], function (err, result)
  // {
  //     if (err) {
  //         console.log("err:", err);
  //     } else {
  //         console.log("results:", result);
  //     }
  // });
  //var connection = new db.Connection(config, function(err) {
  //var request = new db.Request(connection);
  //await db.query("SELECT ADDITION(1,2) as result",
  //{ type: Sequelize.QueryTypes.SELECT }).then(
  //await db.query("call finYearTarget('2020')  ",
  //await db.query("call findalldata(14) ",
  let query = "call find_charity_trans_data(" + psrno + ")";
  console.log("query " + query);
  return await db
    .query(query)
    .then((data1) => {
      //console.log(data1)
      return data1;
    })
    .catch((err) => {
      console.log(err);
      return null; 
    });
  // console.log("AFTER FUNCTION ");
  // //console.log("results:", result);
  // console.log(JSON.stringify(recordset));
  // console.log(recordsets.length);
  //console.log(recordsets[0].result);
  //return responseHelper.sendResponse(res, 200, 0, 'finance year data', recordsets[0].result)
}

// join working using procedure to return charity and charity transactions
exports.find_charity_and_trans_details_join_data = async (req, res, next) => {
  //console.log("inside function find_finyear_summary");
  //var psrno = req.body.inputcharityid;
  var psrno = req.body.srno;
  console.log("Get the tran no/srno    " + psrno);

  // db.query("call finYearTarget(?)", [finyearid], function (err, result)
  // {
  //     if (err) {
  //         console.log("err:", err);
  //     } else {
  //         console.log("results:", result);
  //     }
  // });
  //var connection = new db.Connection(config, function(err) {
  //var request = new db.Request(connection);
  //await db.query("SELECT ADDITION(1,2) as result",
  //{ type: Sequelize.QueryTypes.SELECT }).then(
  //await db.query("call finYearTarget('2020')  ",
  //await db.query("call findalldata(14) ",
  let query = "call find_charity_trans_data(" + psrno + ")";
  console.log("query " + query);
  await db
    .query(query)
    .then((data1) => {
      console.log(data1);
      return responseHelper.sendResponse(res, 200, 0, "data success ", data1);
    })
    .catch((err) => {
      console.log(err);
      return responseHelper.sendFailResponse(res, "failed to get  data");
    });
  // console.log("AFTER FUNCTION ");
  // //console.log("results:", result);
  // console.log(JSON.stringify(recordset));
  // console.log(recordsets.length);
  //console.log(recordsets[0].result);
  //return responseHelper.sendResponse(res, 200, 0, 'finance year data', recordsets[0].result)
};

// join of charity and charity transactions
// Included data of charity  using joins/include
exports.find_join_data_final = (req, res, next) => {
  //var  tmpcharityid  = req.body.charityid;
  var  tmpsrno  = req.body.srno;

  console.log(db.models.charity);
  Charity.hasMany(CharityTrans);
  CharityTrans.belongsTo(Charity, { foreignKey: "charityid" });
  //CharityTrans.belongsTo(Charity);

  CharityTrans.findOne({
    attributes: [
      "srno",
      "amount",
      "finyearid",
      "charityid",
       "charity.charityname"
    ],
    //where: { charityid: tmpcharityid , srno: tmpsrno   },
    where: { srno: tmpsrno   },
    include: [
      {
        model: db.models.charity,
        attributes: ['charityname','contactname','ein']
      },
    ],
    //group: ['CharityTransaction.finyearid','finyeardesc','finyearid','start_date','end_date','target_amount'],
  })
    .then((data1) => {
      return responseHelper.sendResponse(res, 200, 0, "all data", data1);
    })
    .catch((err) => {
      console.log(err);
      return responseHelper.sendFailResponse(res, "failed to get data ");
    });
};

//[Sequelize.col('Charity.charityname')],
//attributes: ['Charity.charityid',	'Charity.charityname',	'Charity.ein',	'Charity.contactname',	'Charity.dearwhom',	'Charity.address1',	'Charity.address2',	'Charity.city',	'Charity.postalcode',	'Charity.emailid1',	'Charity.emailid2',	'Charity.phone1',	'Charity.phone2',	'Charity.url',	'Charity.active',	'Charity.remarks',	'Charity.createdate',	'Charity.createdby',	'Charity.state',	'Charity.suggested_amount',	'Charity.amount_sent']

// join of charity and charity transactions
// Included data of charity  using joins/include
exports.find_join_data_old3 = (req, res, next) => {
  const { charityid } = req.params;
  CharityTrans.findOne({
    include: [
      {
        model: db.models.Charity[Sequelize.col("Charity.charityname")],
        //attributes: ['Charity.charityid',	'Charity.charityname',	'Charity.ein',	'Charity.contactname',	'Charity.dearwhom',	'Charity.address1',	'Charity.address2',	'Charity.city',	'Charity.postalcode',	'Charity.emailid1',	'Charity.emailid2',	'Charity.phone1',	'Charity.phone2',	'Charity.url',	'Charity.active',	'Charity.remarks',	'Charity.createdate',	'Charity.createdby',	'Charity.state',	'Charity.suggested_amount',	'Charity.amount_sent']
        //attributes: []
        where: { srno: 19 },
      },
    ],
    //group: ['CharityTransaction.finyearid','finyeardesc','finyearid','start_date','end_date','target_amount'],
  })
    .then((data1) => {
      return responseHelper.sendResponse(res, 200, 0, "all data", data1);
    })
    .catch((err) => {
      console.log(err);
      return responseHelper.sendFailResponse(res, "failed to get data ");
    });
};

// join of charity and charity transactions
// Included data of charity  using joins/include
exports.find_join_data_old1 = (req, res, next) => {
  const { charityid } = req.query;
  CharityTrans.findOne({
    attributes: [
      "srno",
      "charityid",
      "orgid",
      "finyearid",
      "suggested_amount",
      "amount",
      "amount_in_words",
      "modeofpay",
      "paidvia_cc",
      "paid_from",
      "irs_ok",
      "receipt",
      "receipt_quicken",
      "direction_of",
      "wording_direction",
      "courties_copies",
      "date_of_tran",
      "transmite_date",
      "check_date",
      "email_sent",
      "remarks",
      "createdate",
      "createdby",
      //['Charity.charityid',	'Charity.charityname',	'Charity.ein',	'Charity.contactname',	'Charity.dearwhom',	'Charity.address1',	'Charity.address2',	'Charity.city',	'Charity.postalcode',	'Charity.emailid1',	'Charity.emailid2',	'Charity.phone1',	'Charity.phone2',	'Charity.url',	'Charity.active',	'Charity.remarks',	'Charity.createdate',	'Charity.createdby',	'Charity.state',	'Charity.suggested_amount',	'Charity.amount_sent',
      //attributes: [ 'finyearid','target_amount',
      [
        Sequelize.fn("sum", Sequelize.col("CharityTransaction.amount")),
        "total_amount",
      ],
    ],
    where: {
      charityid: charityid,
    },
    include: [
      {
        model: db.models.charity,
        attributes: ["Charity.charityname"],
        //attributes: ['Charity.charityid',	'Charity.charityname',	'Charity.ein',	'Charity.contactname',	'Charity.dearwhom',	'Charity.address1',	'Charity.address2',	'Charity.city',	'Charity.postalcode',	'Charity.emailid1',	'Charity.emailid2',	'Charity.phone1',	'Charity.phone2',	'Charity.url',	'Charity.active',	'Charity.remarks',	'Charity.createdate',	'Charity.createdby',	'Charity.state',	'Charity.suggested_amount',	'Charity.amount_sent']
      },
    ],
    //group: ['CharityTransaction.finyearid','finyeardesc','finyearid','start_date','end_date','target_amount'],
  })
    .then((data1) => {
      return responseHelper.sendResponse(res, 200, 0, "all data", data1);
    })
    .catch((err) => {
      console.log(err);
      return responseHelper.sendFailResponse(res, "failed to get data ");
    });
};





// join working using procedure to return charity and charity transactions summary year wise 
// this function will be called by email to get the details of the object
async function find_charity_trans_details_summary(tempsrno) {
  //console.log("inside function find_finyear_summary");
  //var psrno = req.body.inputcharityid;
  //var psrno = tempsrno;
  var pcharityid = tempsrno;
  console.log("Get the charityid    " + pcharityid);
  //console.log("Get the tran no/srno    " + psrno);
  // db.query("call finYearTarget(?)", [finyearid], function (err, result)
  // {
  //     if (err) {
  //         console.log("err:", err);
  //     } else {
  //         console.log("results:", result);
  //     }
  // });
  //var connection = new db.Connection(config, function(err) {
  //var request = new db.Request(connection);
  //await db.query("SELECT ADDITION(1,2) as result",
  //{ type: Sequelize.QueryTypes.SELECT }).then(
  //await db.query("call finYearTarget('2020')  ",
  //await db.query("call findalldata(14) ",
  let query = "call find_charity_trans_summ(" + pcharityid + ")";
  console.log("query " + query);
  return await db
    .query(query)
    .then((data1) => {
      //console.log(data1)
      return data1;
    })
    .catch((err) => {
      console.log(err);
      return null;
    });
  // console.log("AFTER FUNCTION ");
  // //console.log("results:", result);
  // console.log(JSON.stringify(recordset));
  // console.log(recordsets.length);
  //console.log(recordsets[0].result);
  //return responseHelper.sendResponse(res, 200, 0, 'finance year data', recordsets[0].result)
}