const db = require('../../configs/database');
const FinYears = require('../Models/Finyears');
const Op = require('sequelize').Op;
const responseHelper = require('../../helpers/ApiResponseHelper');

var log = function(inst){
    console.dir(inst.get())
}


// find all 
exports.find_all = (req, res, next) => {

    FinYears.findAll().then((finyears) => {
       return responseHelper.sendResponse(res, 200, 0, 'finance year data', finyears)
   }).catch((err) => {
       return responseHelper.sendFailResponse(res, 'failed to get finance year data');
   })
 }
 

//Create and Update
exports.create_and_update = (req, res, next) => {
  var body = req.body;
  console.log("test create ");
  console.log(body);

  if (body.seqno && body.seqno > 0) 
  {
      console.log("test create 11 - " + body.finyearid );
      FinYears.update(body, {
          where: { finyearid: body.finyearid }
      }).then((finledger) => {
          console.log("test create success - " + body.finyearid );
          responseHelper.sendSuccessResponse(res, 'finance year updated successfully', null)
      }).catch((err) => {
          console.log("test update fail - " + body.finyearid );
          console.log(err);
          responseHelper.sendFailResponse(res, 'failed to update finance year');
                    
      });
  } else {
      FinYears.create(body).then((finyears) => {
          return responseHelper.sendSuccessResponse(res, 'finance year created successfully', finyears)
      }).catch((err) => {
          console.log(err);
          return responseHelper.sendFailResponse(res, 'failed to create finance year');
      })
  }
}










// Dynamic where clause depend upon the inputs 
// as of now only 1 id or 1 name is supported in input 
// multiple supported ..code fixed 
exports.find_all_filter = (req, res, next) => {
    //var charityname = req.params.charityname;
    //var charityid = req.params.charityid;
  
    console.log("inside function find_one_idandname_filter");
    //var transnolist = req.body.transnolist;
    //var charityidlist = req.body.charityidlist;
    //var orgid = req.body.orgid;
    var finyearid = req.body.finyearid;
  
    //console.log("Get the trans no length   " + transnolist.length   );
    //console.log("Get the trans no list   " + transnolist);
    //console.log("Get the charity id list " + charityidlist);
    //console.log("Get the orgidid id      " + orgid);
    console.log("Get the finyearid id    " + finyearid);
  
    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }
  
    // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
    //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    // }).catch((err) => {
    //     return responseHelper.sendFailResponse(res, 'failed to get charity');
    // })
  
    //console.dir(transnolist);
    //console.dir(charityidlist);
  
    let queryObject = {};
    queryObject.where = {};
  
    //if(charityidlist && transnolist )
    if (finyearid.length > 0 && finyearid.length > 0) {
      console.log("finyearid search ");
     // queryObject.where.charityid = { [Op.in]: charityidlist };
      //queryObject.where.srno = { [Op.in]: transnolist };
      queryObject.where.finyearid = { [Op.in]: finyearid };
      //queryObject.where.orgid = { [Op.in]: orgid };
    }

    // } else if (transnolist.length > 0) {
    //   console.log("only trans no ");
    //   queryObject.where.srno = { [Op.in]: transnolist };
    //   queryObject.where.finyearid = { [Op.in]: finyearid };
    //   queryObject.where.orgid = { [Op.in]: orgid };
    // } else if (charityidlist.length > 0) {
    //   console.log("only charity id ");
    //   queryObject.where.charityid = { [Op.in]: charityidlist };
    //   queryObject.where.finyearid = { [Op.in]: finyearid };
    //   queryObject.where.orgid = { [Op.in]: orgid };
    // } else {
    //   console.log("only fin year and org id ");
    //   queryObject.where.finyearid = { [Op.in]: finyearid };
    //   queryObject.where.orgid = { [Op.in]: orgid };
    // }
  
  
    FinYears.findAll(queryObject).then((finyears) => {
      return responseHelper.sendSuccessResponse(res, 'finyears details', finyears)
    }).catch((err) => {
      return responseHelper.sendFailResponse(res, 'failed to get finyears details');
    })
  
  
  }
  





  