const db = require('../../configs/database');
const Users = require('../Models/Users');
const moment = require('moment');
const bcrypt = require('bcrypt');
const Op = require('sequelize').Op;
const jwt = require('jsonwebtoken');
require('../../configs/authentication');
var _ = require('lodash');
const responseHelper = require('../../helpers/ApiResponseHelper');
const refreshTokens = {};
const SECRET = process.env.passport_secrets;
const randtoken = require('rand-token');
const { body, validationResult } = require("express-validator");

exports.create_user = (req, res, next) => {
    var body = req.body;
    console.log(" user creation -  ");

    if (body.userId && body.userId > 0) {
        delete body.password;

        Users.update(body, {
            where: { userId: body.userId }
        }).then((user) => {
            responseHelper.sendSuccessResponse(res, 'user updated successfully', null)
        }).catch((err) => {
            responseHelper.sendFailResponse(res, 'failed to update user');
        });

    } else {
        Users.findOne({
            where: {
                [Op.or]: [{ phone_no: body.phone_no, email: body.email }]
            }
        }).then(isUserExists => {
            if (isUserExists) {
                return responseHelper.sendFailResponse(res, 'user already exists');
            } else {
                bcrypt.hash(body.password, 10, (err, hash) => {
                    if (err) {
                        return responseHelper.sendFailResponse(res, 'failed to create user 1 ');
                    } else {
                        body.password = hash;
                        Users.create(body).then((user) => {
                            return responseHelper.sendSuccessResponse(res, 'user created successfully', user)
                        }).catch((err) => {
                            return responseHelper.sendFailResponse(res, 'failed to create user 2');
                        })
                    }
                });
            }
        }).catch((err) => {
            return responseHelper.sendFailResponse(res, 'failed to create user');
        });
    }

}

exports.get_all_users = (req, res, next) => {
    Users.findAll({
        where: { status: 1 },
        attributes: ['userId', 'user_name', 'user_type', 'email', 'phone_no']
    }).then((user) => {
        return responseHelper.sendResponse(res, 200, 0, 'users data', user)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get users');
    })
}



exports.get_one_userdetails = (req, res, next) => {

    console.log("get_one_users - function ");
    var pemail = req.body.email;
    console.log("email - "+ pemail);

    Users.findOne ({        
        where: {
             email: pemail 
            //status: 1
        },
        attributes: ['userId', 'user_name', 'user_type', 'email', 'phone_no','status']
    }).then((user) => {
        return responseHelper.sendResponse(res, 200, 0, 'users data', user)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get users');
    })
}


exports.delete_user = (req, res, next) => {
    var userId = req.params.userId;
    Users.destroy({
        where: { status: userId }
    }).then((user) => {
        return responseHelper.sendSuccessResponse(res, 'user deleted successfully', user)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to delete user');
    })
}

exports.sign_in = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors });
    }

    data = req.body;
    Users.findOne({
        where: {
            status: 1,
            email: data.email
        },
    }).then(isUser => {
        if (isUser) {
            bcrypt.compare(data.password, isUser.password, (err, result) => {
                if (err) {
                    return responseHelper.sendFailResponse(res, 'Auth failed invalid password');
                }
                if (result) {
                    delete isUser.dataValues.password;
                    var jsonToken = jsonTokenGenerate(isUser);
                    const data = {
                        user: isUser,
                        token: jsonToken
                    }
                    return responseHelper.sendResponse(res, 200, 0, 'user authenticated', data);
                }
                return responseHelper.sendFailResponse(res, 'Auth failed invalid password');
            })
        } else {
            return responseHelper.sendFailResponse(res, 'Auth failed invalid user');
        }
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'Internal server error');
    })
}

function jsonTokenGenerate(userData) {
    const user = {
        userId: userData.userId,
        userType: userData.user_type
    };
    console.log(user)
    const token = jwt.sign(user, SECRET, { expiresIn: '60m' })
    const refreshToken = randtoken.uid(256);
    refreshTokens[refreshToken] = userData.userId;

    return {
        jwt: token,
        refreshToken: refreshToken
    }
}


