const db = require('../../configs/database');
const FinLedger = require('../Models/FinLedger');
const Op = require('sequelize').Op;
const responseHelper = require('../../helpers/ApiResponseHelper');

var log = function(inst){
    console.dir(inst.get())
}

// bulk create and update
//Create and Update
exports.bulk_create_and_update = (req, res, next) => {
    var body = req.body;
    console.log("test create ");
    console.log(body);
    var tmpseqno= body.seqno;
    var tmpfinyearid= body.finyearid;
    
  
    if (body.seqno && body.seqno > 0) 
    {
        console.log("test seqno - " + body.seqno );
        console.log("test finyear - " + body.finyearid );
        FinLedger.bulkCreate     (body, {
            where: { seqno: tmpseqno , finyearid: tmpfinyearid  }
        }).then((finledger) => {
            console.log("test update success - " + body.seqno );
            responseHelper.sendSuccessResponse(res, 'FinLedger updated successfully', finledger)
        }).catch((err) => {
            console.log("test update fail - " + body.seqno );
            console.log(err);
            responseHelper.sendFailResponse(res, 'failed to update FinLedger');
                      
        });
    } else {
        FinLedger.bulkCreate(body, 
            {
                updateOnDuplicate:[ 'amount',      body.amount,
                                    'description', body.description,
                                    'remarks',     body.remarks]               
            }            
            ).then((finledger) => {
            return responseHelper.sendSuccessResponse(res, 'FinLedger created successfully', finledger)
        }).catch((err) => {
            console.log(err);
            return responseHelper.sendFailResponse(res, 'failed to create FinLedger');
        })
    }
  }


// find all 
exports.find_all = (req, res, next) => {

    console.log("test in ") 
    FinLedger.findAll().then((finledger) => {
       return responseHelper.sendResponse(res, 200, 0, 'finance ledger data', finledger)
   }).catch((err) => {
       return responseHelper.sendFailResponse(res, 'failed to get finance ledger data');
   })
 }
 

//Create and Update
exports.create_and_update = (req, res, next) => {
  var body = req.body;
  console.log("test create ");
  console.log(body);
  var tmpseqno= body.seqno;
  var tmpfinyearid= body.finyearid;
  

  if (body.seqno && body.seqno > 0) 
  {
      console.log("test seqno - " + body.seqno );
      console.log("test finyear - " + body.finyearid );
      FinLedger.update(body, {
          where: { seqno: tmpseqno , finyearid: tmpfinyearid  }
      }).then((finledger) => {
          console.log("test update success - " + body.seqno );
          responseHelper.sendSuccessResponse(res, 'FinLedger updated successfully', finledger)
      }).catch((err) => {
          console.log("test update fail - " + body.seqno );
          console.log(err);
          responseHelper.sendFailResponse(res, 'failed to update FinLedger');
                    
      });
  } else {
      FinLedger.create(body).then((finledger) => {
          return responseHelper.sendSuccessResponse(res, 'FinLedger created successfully', finledger)
      }).catch((err) => {
          console.log(err);
          return responseHelper.sendFailResponse(res, 'failed to create FinLedger');
      })
  }
}










// Dynamic where clause depend upon the inputs 
// as of now only 1 id or 1 name is supported in input 
// multiple supported ..code fixed 
exports.find_all_ledger_filter = (req, res, next) => {
    //var charityname = req.params.charityname;
    //var charityid = req.params.charityid;
  
    console.log("inside function find_one_idandname_filter");
    //var transnolist = req.body.transnolist;
    //var charityidlist = req.body.charityidlist;
    //var orgid = req.body.orgid;
    var finyearid = req.body.finyearid;
  
    //console.log("Get the trans no length   " + transnolist.length   );
    //console.log("Get the trans no list   " + transnolist);
    //console.log("Get the charity id list " + charityidlist);
    //console.log("Get the orgidid id      " + orgid);
    console.log("Get the finyearid id    " + finyearid);
  
    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }
  
    // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
    //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    // }).catch((err) => {
    //     return responseHelper.sendFailResponse(res, 'failed to get charity');
    // })
  
    //console.dir(transnolist);
    //console.dir(charityidlist);
  
    let queryObject = {};
    queryObject.where = {};
  
    //if(charityidlist && transnolist )
    if (finyearid.length > 0 && finyearid.length > 0) {
      console.log("finyearid search ");
     // queryObject.where.charityid = { [Op.in]: charityidlist };
      //queryObject.where.srno = { [Op.in]: transnolist };
      queryObject.where.finyearid = { [Op.in]: finyearid };
      //queryObject.where.orgid = { [Op.in]: orgid };
    }

    // } else if (transnolist.length > 0) {
    //   console.log("only trans no ");
    //   queryObject.where.srno = { [Op.in]: transnolist };
    //   queryObject.where.finyearid = { [Op.in]: finyearid };
    //   queryObject.where.orgid = { [Op.in]: orgid };
    // } else if (charityidlist.length > 0) {
    //   console.log("only charity id ");
    //   queryObject.where.charityid = { [Op.in]: charityidlist };
    //   queryObject.where.finyearid = { [Op.in]: finyearid };
    //   queryObject.where.orgid = { [Op.in]: orgid };
    // } else {
    //   console.log("only fin year and org id ");
    //   queryObject.where.finyearid = { [Op.in]: finyearid };
    //   queryObject.where.orgid = { [Op.in]: orgid };
    // }
  
  
    FinLedger.findAll(queryObject).then((finledger) => {
      return responseHelper.sendSuccessResponse(res, 'ledger details', finledger)
    }).catch((err) => {
      return responseHelper.sendFailResponse(res, 'failed to get ledger details');
    })
  
  }




// Dynamic where clause depend upon the inputs 
// as of now only 1 id or 1 name is supported in input 
// multiple supported ..code fixed 
exports.find_all_year_ledger_filter = (req, res, next) => {
  
    console.log("inside function find_all_year_ledger_filter ");
    var finyearid = req.body.finyearid;
    console.log("Get the finyearid id    " + finyearid);
  
    var finledger =  find_footer_year_details_summary(finyearid);
    console.log("fin ledger data after function");
    console.log(finledger);
    //var retstr = await emailtocharity(charityTrans[i]);

    // FinLedger.findAll().then((finledgerdummy ) => {
    //   return responseHelper.sendSuccessResponse(res, 'ledger details', finledger)
    // }).catch((err) => {
    //   return responseHelper.sendFailResponse(res, 'failed to get ledger details');
    // })  
  }

  
    // new function for finding the footer data by calling stored procedure 
    // join working using procedure to return charity and charity transactions summary year wise 
    // this function will be called by email to get the details of the object
    async function find_footer_year_details_summary(yearid) {
    //console.log("inside function find_finyear_summary");
    //var psrno = req.body.inputcharityid;
    //var psrno = tempsrno;
    var pyearid = yearid;
    console.log("Get the yearid    " + pyearid);
    //console.log("Get the tran no/srno    " + psrno);
    // db.query("call finYearTarget(?)", [finyearid], function (err, result)
    // {
    //     if (err) {
    //         console.log("err:", err);
    //     } else {
    //         console.log("results:", result);
    //     }
    // });
    //var connection = new db.Connection(config, function(err) {
    //var request = new db.Request(connection);
    //await db.query("SELECT ADDITION(1,2) as result",
    //{ type: Sequelize.QueryTypes.SELECT }).then(
    //await db.query("call finYearTarget('2020')  ",
    //await db.query("call findalldata(14) ",
    let query = "call find_footer_summ(" + pyearid + ")";
    console.log("query " + query);
    return await db
      .query(query)
      .then((data1) => {
        //console.log(data1)
        return data1;
      })
      .catch((err) => {
        console.log(err);
        return null;
      });
    // console.log("AFTER FUNCTION ");
    // //console.log("results:", result);
    // console.log(JSON.stringify(recordset));
    // console.log(recordsets.length);
    //console.log(recordsets[0].result);
    //return responseHelper.sendResponse(res, 200, 0, 'finance year data', recordsets[0].result)
  }
  


  // Dynamic where clause depend upon the inputs 
// as of now only 1 id or 1 name is supported in input 
// multiple supported ..code fixed  
exports.find_year_ledger_footer = async (req, res, next) => {
  
    console.log("inside function find_all_year_ledger_filter ");
    var finyearid = req.body.finyearid;
    console.log("Get the finyearid id    " + finyearid);
  
    let query = "call find_footer_summ('" + finyearid + "')";
    console.log("query " + query);
    return await db.query(query).then((data1) => {
        return responseHelper.sendSuccessResponse(res, 'ledger details', data1)
      }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get ledger details');
      })        
 
  }