const db = require('../../configs/database');
const Charity = require('../Models/Charity');
const Op = require('sequelize').Op;
const responseHelper = require('../../helpers/ApiResponseHelper');

var log = function(inst){
    console.dir(inst.get())
}


//Create and Update
exports.create_and_update = (req, res, next) => {
    var body = req.body;
    console.log("test create ");
    console.log(body);

    if (body.charityid && body.charityid > 0) {
        Charity.update(body, {
            where: { charityid: body.charityid }
        }).then((charity) => {
            responseHelper.sendSuccessResponse(res, 'charity updated successfully', null)
        }).catch((err) => {
            responseHelper.sendFailResponse(res, 'failed to update charity');
        });
    } else {
        Charity.create(body).then((charity) => {
            return responseHelper.sendSuccessResponse(res, 'charity created successfully', charity)
        }).catch((err) => {
            return responseHelper.sendFailResponse(res, 'failed to create charity');
        })
    }
}



exports.delete = (req, res, next) => {
    var charityid = req.params.charityid;
    console.log(charityid)
    Charity.destroy({
        where: { charityid: charityid }
    }).then((charities) => {
        return responseHelper.sendSuccessResponse(res, 'charity deleted successfully', charities)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to delete charity');
    })
}

// find all 
exports.find_all = (req, res, next) => {
    Charity.findAll().then((charities) => {
        return responseHelper.sendResponse(res, 200, 0, 'charities trans data', charities)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charities trans');
    })
}


// find all - exact search with 1 string
exports.find_all_idname = (req, res, next) => {
    Charity.findAll( { attributes:["charityid","charityname"] } ).then((charities) => {
        return responseHelper.sendResponse(res, 200, 0, 'charities trans data', charities)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charities trans');
    })
}

exports.find_one = (req, res, next) => {
    var charityid = req.params.charityid;
    console.dir(charityid);
    //log=charityid;

    Charity.findByPk(charityid).then((charity) => {
        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
    })
}

exports.find_one_name = (req, res, next) => {
    var charityname = req.params.charityname;
    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }

    Charity.findAll(
        { where:{        
             charityname: { [Op.like]: '%' + charityname + '%' },
            }}      
    ).then((charity) => {
        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
    })
}

exports.delete_all = (req, res, next) => {
    Charity.truncate().then((charity) => {
        return responseHelper.sendSuccessResponse(res, 'charity table truncated successfully', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to truncated charity table');
    })
}


exports.find_one_name_filter1 = (req, res, next) => {
    var charityname = req.params.charityname;
    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }

    Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
    })
}


exports.find_all_name_filtermany = (req, res, next) => {
    var charitylist =    req.body.charitynamelist;
    
    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }

    // charityname: { [Op.or]: [charityIdArr])

    Charity.findAll({ 
           where:{ charityname: { [Op.or]: [charitylist]} }}  ).then((charity) => {
        //console.log(charity);
        // for (let i = 0; i < charity.length; i++)  
        // {
        //     console.log(charity[i].dataValues.charityid  );
        //     console.log(charity[i].dataValues.charityname  );
        // }
        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
    })
}

// as of now only 1 id and 1 name is supported in input 
exports.find_one_idandname_filter = (req, res, next) => {
    //var charityname = req.params.charityname;
    //var charityid = req.params.charityid;

    console.log("inside function find_one_idandname_filter");
    var charitynamelist =    req.body.charitynamelist;
    var charityidlist   =    req.body.charityidlist;
    console.log("Get the charitynamelist " + charitynamelist);

    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }

    // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
    //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    // }).catch((err) => {
    //     return responseHelper.sendFailResponse(res, 'failed to get charity');
    // })

    console.dir(charitynamelist);
    console.dir(charityidlist);
    
    Charity.findAll(
    { 
        where:{ 
            charityname:  charitynamelist,
            charityid:    parseInt(charityidlist)                        
              }
    }  ).then((charity) => {
     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
     return responseHelper.sendFailResponse(res, 'failed to get charity');
    })

}


//Dynamic where clause depend upon the inputs 
// as of now only 1 id or 1 name is supported in input 
exports.find_one_idorname_dyn_filter = (req, res, next) => {
    //var charityname = req.params.charityname;
    //var charityid = req.params.charityid;

    console.log("inside function find_one_idandname_filter");
    var charitynamelist =    req.body.charitynamelist;
    var charityidlist   =    req.body.charityidlist;
    console.log("Get the charitynamelist " + charitynamelist);

    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }

    // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
    //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    // }).catch((err) => {
    //     return responseHelper.sendFailResponse(res, 'failed to get charity');
    // })

    console.dir(charitynamelist);
    console.dir(charityidlist);

    let queryObject = {};
    queryObject.where = {};

    if(charityidlist && charitynamelist )
      {
        console.log("id and name ");
        queryObject.where.charityid   = { [Op.eq]: parseInt(charityidlist) };
        queryObject.where.charityname = { [Op.eq]: charitynamelist };
      } else if (charitynamelist)
      {
        console.log("only name ");
        queryObject.where.charityname = { [Op.like]: charitynamelist  };
      } else if (charityidlist)
      {
        console.log("only id ");
        queryObject.where.charityid = { [Op.eq]: parseInt(charityidlist) };
      }


      Charity.findAll(queryObject).then((charity) => {
        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
       }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
       })

    
    // Charity.findAll(
    // { 
    //     where:{ 
    //         charityname:  charitynamelist,
    //         charityid:    parseInt(charityidlist)                        
    //           }
    // }  ).then((charity) => {
    //  return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    // }).catch((err) => {
    //  return responseHelper.sendFailResponse(res, 'failed to get charity');
    // })


    // var body = req.body;
    // if (body.charityid && body.charityid > 0) {


    //     var charitynamelist =    req.body.charitynamelist;
    //     var charityidlist   =    req.body.charityidlist;

}


exports.find_one_email = (req, res, next) => {
    var charityid = req.params.charityid;
    console.dir(charityid);
    var nodemailer = require('nodemailer');

    //log=charityid;

    Charity.findByPk(charityid).then((charity) => {

        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'skillnetincmail@gmail.com',
              pass: 'skill@1234'
            }
          });
          
          var mailOptions = {
            from: 'skillnetincmail@gmail.com',
            to: 'harishbogawat@gmail.com',
            subject: 'Sending Email using Node.js',
            //text: 'That was easy!'
            html: '<h1>Welcome</h1><p>That was easy!</p>'

          };
          
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });


        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
    })

}


exports.find_all_name_update_all = (req, res, next) => {
    var charitylist =    req.body.charitynamelist;
    
    //const { charityname } = req.body;
    // Charity.findOne({ charityname }, function (err, charity) {
    //   if (!charity) {
    //     return res.status(400).json({
    //       err: "charity name does not exist",
    //     });
    //   }

//    charityname: { [Op.or]: [charityIdArr])


    Charity.findAll({ 
           where:{ charityname: { [Op.or]: [charitylist]} }}  ).then((charity) => {
        //console.log(charity);

        for (let i = 0; i < charity.length; i++)  
        {
            console.log(charity[i].dataValues.charityid  );
            console.log(charity[i].dataValues.charityname  );

            if (charity[i].dataValues.charityid > 0) 
            {
                
                //console.log(addn(1,2));
                //let retstr=emailtocharity(charity[i].dataValues.charityname ) ;
                //emailtocharity(charity[i].dataValues.charityname,function(retstr) ) ;
                emailtocharity(charity[i].dataValues.charityname) ;
                //console.log("Email Function returns " + retstr);

                Charity.update(
                {
                     remarks:'a very different remarks now',
                     createdate:Date.now() 
                }, 
                {
                    where: { charityid: charity[i].dataValues.charityid }
                }).then(count => {
                    console.log('Rows updated ' + count);
                });
            }


        }

        return responseHelper.sendSuccessResponse(res, 'charity details', charity)
    }).catch((err) => {
        return responseHelper.sendFailResponse(res, 'failed to get charity');
    })
}


// Function Email Pass the Name as a Paramater
 function emailtocharity_nodemailer(strname)  {

    var isValid = "Y";
    var retstr="0";
    var nodemailer = require('nodemailer');

    console.log('Inside Email Function for  user  ' + strname);
    //send email code 
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'skillnetincmail@gmail.com',
          pass: 'skill@1234'
        }
      });
      
      var mailOptions = {
        from: 'skillnetincmail@gmail.com',
        to: 'harishbogawat@gmail.com',
        subject: 'Sending Email using Node.js for id '+ strname,
        //text: 'That was easy!'
        html: '<h1>Welcome</h1><p>That was easy!</p>'

      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
          retstr="0";
          return retstr;
          //callback(retstr);
        } else {
          console.log('Email sent: ' + info.response);
          retstr="1";
          return retstr;
          //callback(retstr) ;
        }
      });
      //send email code is done     

    // if (isValid) {
    //     //next();
    //     return;
    // } else {
    //     //res.send("please try again");
    //     return;
    // }
    
}




// SequlizeModel.findOne({where: {id: 'some-id'}})
// .then(record => {
  
//   if (!record) {
//     throw new Error('No record found')
//   }

//   console.log(`retrieved record ${JSON.stringify(record,null,2)}`) 

//   let values = {
//     registered : true,
//     email: 'some@email.com',
//     name: 'Joe Blogs'
//   }
  
//   record.update(values).then( updatedRecord => {
//     console.log(`updated record ${JSON.stringify(updatedRecord,null,2)}`)
//     // login into your DB and confirm update
//   })

// })
// .catch((error) => {
//   // do seomthing with the error
//   throw new Error(error)
// })




// function addn(a,b)  {
//     console.log("in add n function ")
//     console.log(a+b);
//     callbackPromise(a+b); 
// }


// const https = require('https')
// var url = 'https://api.coinbase.com/v2/prices/spot?currency=USD';

// function makeCall (url) {
// return new Promise((resolve, reject) => {
//     https.get(url,function (res) {
//         res.on('data', function (d) {
//             resolve(JSON.parse(d));
//         });
//         res.on('error', function (e) {
//             reject(e)
//         });
//     });
// })

// }

// function handleResults(results){
// return Number((results.data.amount))*14.5;
// }




// Function Email Pass the Name as a Paramater
function emailtocharity(strname)  
{
    var isValid = "Y";
    var retstr="0";
    console.log('Inside Email Function for  mailjet - user  ' + strname);

const mailjet = require ('node-mailjet')
.connect('700812e8024314adad98321596d51452', '3d8b78d4034519f4137473c21915c306')
const request = mailjet
.post("send", {'version': 'v3.1'})
.request({
  "Messages":[
    {
      "From": {
        "Email": "harishbogawat@gmail.com",
        "Name": "Harish"
      },
      "To": [
        {
          "Email": "harishbogawat@gmail.com",
          "Name": "Harish"
        }
      ],
      "Subject": "Greetings from Mailjet.",
      "TextPart": "My first Mailjet email",
      "HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
      "CustomID": "AppGettingStartedTest"
    }
  ]
})
request
  .then((result) => {
    console.log(result.body)
  })
  .catch((err) => {
    console.log(err.statusCode)
  })

}