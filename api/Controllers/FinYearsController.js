const db = require('../../configs/database');
const FinYears = require('../Models/Finyears');
const CharityTransactions = require('../Models/CharityTransactions');
const Op = require('sequelize').Op;
const responseHelper = require('../../helpers/ApiResponseHelper');
const Sequelize = require('sequelize');

var log = function (inst) {
  console.dir(inst.get())
}
 
// find all 
exports.find_all = (req, res, next) => {
  //const { finYearId } = req.params;
  FinYears.findAll (
    {
        attributes: [ 'finyeardesc','finyearid','start_date','end_date','target_amount',
        [Sequelize.fn('sum', Sequelize.col('CharityTransaction.amount')), 'total_amount'],
                    ],
      // where: {
      //   finyearid: finYearId
      // },
      include: [{
         model: db.models.CharityTransactions,
         attributes: []
       }],
       //group: ['CharityTransaction.finyearid'],
       group: ['CharityTransaction.finyearid','finyeardesc','finyearid','start_date','end_date','target_amount'],
    }
  ).then((finyears) => {
    return responseHelper.sendResponse(res, 200, 0, 'finance year data', finyears)
  }).catch((err) => {
    console.log(err)
    return responseHelper.sendFailResponse(res, 'failed to get finance year data');
  })
}

// find one year details 
// Included data of charity transaction using group by function
exports.find_one = (req, res, next) => {
  const { finYearId } = req.params;
  FinYears.findOne(
    {
        attributes: [ 'finyeardesc','finyearid','start_date','end_date','target_amount',
        //attributes: [ 'finyearid','target_amount',
        [Sequelize.fn('sum', Sequelize.col('CharityTransaction.amount')), 'total_amount'],
                    ],
      where: {
        finyearid: finYearId
      },
      include: [{
         model: db.models.CharityTransactions,
         attributes: []
       }],
       group: ['CharityTransaction.finyearid','finyeardesc','finyearid','start_date','end_date','target_amount'],
    }
  ).then((finyears) => {
    return responseHelper.sendResponse(res, 200, 0, 'finance year data', finyears)
  }).catch((err) => {
    console.log(err)
    return responseHelper.sendFailResponse(res, 'failed to get finance year data');
  })
}


// find one year details and one charity id details 
// new function for footer on charity transaction charity id wise 
exports.find_finyear_charity_summary = (req, res, next) => {
  //const { finYearId } = req.params;
  //const { charityId } = req.params;
  var body = req.body;
  console.log(body);
  const  finYearId  = body.finYearId;
  const  charityId  = body.charityId;

  console.log(" Fin Year id " + finYearId);
  console.log(" charityId   " + charityId);

  FinYears.findOne(
    {
        attributes: [ 'finyeardesc','finyearid','start_date','end_date','target_amount',
        [Sequelize.fn('sum', Sequelize.col('CharityTransaction.amount')), 'total_amount'],
                    ],
      where: {
        finyearid: finYearId
      },
      include: [{
         model: db.models.CharityTransactions,
         attributes: [],
         where: {
          charityid: charityId
        },
       }],
       group: ['CharityTransaction.finyearid','CharityTransaction.charityid'],
    }
  ).then((finyears) => {
    return responseHelper.sendResponse(res, 200, 0, 'finance year data', finyears)
  }).catch((err) => {
    console.log(err)
    return responseHelper.sendFailResponse(res, 'failed to get finance year data');
  })
}


//Create and Update
exports.create_and_update = (req, res, next) => {
  var body = req.body;
  console.log("test create ");
  console.log(body);

  if (body.finyearid && body.finyearid > 0) {
    console.log("test create 11 - " + body.finyearid);
    FinYears.update(body, {
      where: { finyearid: body.finyearid }
    }).then((finledger) => {
      console.log("test create success - " + body.finyearid);
      responseHelper.sendSuccessResponse(res, 'finance year updated successfully', null)
    }).catch((err) => {
      console.log("test update fail - " + body.finyearid);
      console.log(err);
      responseHelper.sendFailResponse(res, 'failed to update finance year');
    });
  } else {
    FinYears.create(body).then((finyears) => {
      return responseHelper.sendSuccessResponse(res, 'finance year created successfully', finyears)
    }).catch((err) => {
      console.log(err);
      return responseHelper.sendFailResponse(res, 'failed to create finance year');
    })
  }
}










// Dynamic where clause depend upon the inputs 
// as of now only 1 id or 1 name is supported in input 
// multiple supported ..code fixed 
exports.find_all_filter = (req, res, next) => {
  //var charityname = req.params.charityname;
  //var charityid = req.params.charityid;

  console.log("inside function find_one_idandname_filter");
  //var transnolist = req.body.transnolist;
  //var charityidlist = req.body.charityidlist;
  //var orgid = req.body.orgid;
  var finyearid = req.body.finyearid;

  //console.log("Get the trans no length   " + transnolist.length   );
  //console.log("Get the trans no list   " + transnolist);
  //console.log("Get the charity id list " + charityidlist);
  //console.log("Get the orgidid id      " + orgid);
  console.log("Get the finyearid id    " + finyearid);

  //const { charityname } = req.body;
  // Charity.findOne({ charityname }, function (err, charity) {
  //   if (!charity) {
  //     return res.status(400).json({
  //       err: "charity name does not exist",
  //     });
  //   }

  // Charity.findOne({ where:{charityname:charityname}  }).then((charity) => {
  //     return responseHelper.sendSuccessResponse(res, 'charity details', charity)
  // }).catch((err) => {
  //     return responseHelper.sendFailResponse(res, 'failed to get charity');
  // })

  //console.dir(transnolist);
  //console.dir(charityidlist);

  let queryObject = {};
  queryObject.where = {};

  //if(charityidlist && transnolist )
  if (finyearid.length > 0 && finyearid.length > 0) {
    console.log("finyearid search ");
    // queryObject.where.charityid = { [Op.in]: charityidlist };
    //queryObject.where.srno = { [Op.in]: transnolist };
    queryObject.where.finyearid = { [Op.in]: finyearid };
    //queryObject.where.orgid = { [Op.in]: orgid };
  }

  // } else if (transnolist.length > 0) {
  //   console.log("only trans no ");
  //   queryObject.where.srno = { [Op.in]: transnolist };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.orgid = { [Op.in]: orgid };
  // } else if (charityidlist.length > 0) {
  //   console.log("only charity id ");
  //   queryObject.where.charityid = { [Op.in]: charityidlist };
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.orgid = { [Op.in]: orgid };
  // } else {
  //   console.log("only fin year and org id ");
  //   queryObject.where.finyearid = { [Op.in]: finyearid };
  //   queryObject.where.orgid = { [Op.in]: orgid };
  // }


  FinYears.findAll(queryObject).then((finyears) => {
    return responseHelper.sendSuccessResponse(res, 'finyears details', finyears)
  }).catch((err) => {
    return responseHelper.sendFailResponse(res, 'failed to get finyears details');
  })


}




















exports.find_finyear_summary = async (req, res, next) => {

  //console.log("inside function find_finyear_summary");
  var finyearid = req.body.finyearid;
  console.log("Get the finyearid id    " + finyearid);

  // db.query("call finYearTarget(?)", [finyearid], function (err, result) 
  // {
  //     if (err) {
  //         console.log("err:", err);
  //     } else {
  //         console.log("results:", result);
  //     }

  // });
  //     //var connection = new db.Connection(config, function(err) {
  //var request = new db.Request(connection);
  //await db.query("SELECT ADDITION(1,2) as result",
  //{ type: Sequelize.QueryTypes.SELECT }).then(
  //await db.query("call finYearTarget('2020')  ",
  await db.query("call findalldata(14)  ",

  ).then(
    (finyears) => {
      console.log(finyears)
      return responseHelper.sendResponse(res, 200, 0, 'finance year data', finyears)
    }).catch((err) => {
      console.log(err);
      return responseHelper.sendFailResponse(res, 'failed to get finance year data');
    })
  // console.log("AFTER FUNCTION ");
  // //console.log("results:", result);
  // console.log(JSON.stringify(recordset));
  // console.log(recordsets.length);
  //console.log(recordsets[0].result);
  //return responseHelper.sendResponse(res, 200, 0, 'finance year data', recordsets[0].result)
}



// Function Email Pass the Name as a Paramater
async function finyeartargetamount(finyearid) {

  var isValid = "Y";
  var retstr = "0";

  const msg = {
    to: 'harishbogawat@gmail.com', // Change to your recipient
    from: 'harish.kumar@skillnetinc.com', // Change to your verified sender
    subject: 'Sending with SendGrid is Fun -test ',
    text: 'and easy to do anywhere, even with Node.js',
    html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  }
  return sgMail
    .send(msg)
    .then(() => {
      console.log('Email sent')
      return (1);
    })
    .catch((error) => {
      console.error(error)
      return (0);
    })
}




//'srno','charityid','orgid','finyearid','suggested_amount','amount','amount_in_words','modeofpay','paidvia_cc','paid_from','irs_ok'receipt'receipt_quicken','direction_of','wording_direction'courties_copies','date_of_tran','transmite_date','check_date','email_sent','remarks','createdate','createdby',
