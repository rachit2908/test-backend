module.exports = {


    sendUnauthorizedResponse: (res, data) => {
        res.status("401").send({
            success: false,
            message: "unauthorized access",
            data: data,
        });
    },

    sendResponse: (res, statusCode, success, message, data) => {
        res.status(statusCode).json({
            success: success,
            message: message,
            data: data,
        });
    },

    sendSuccessResponse: (res, message, data) => {
        res.status("200").send({
            success: 0,
            message: message ? message : "success",
            data: data,
        });
    },

    sendFailResponse: (res, message) => {
        res.status(400).json({
            success: -1,
            message: message ? message : "unable to process data",
        });
    },

    sendCatchResponse: (res, message) => {
        res.status("500").send({
            success: false,
            message: message ? message : "internal server error",
        });
    },

};
