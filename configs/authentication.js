const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const jwt = require('jsonwebtoken');
const User = require('../api/Models/Users');

module.exports = function (passport) {
    const SECRET = process.env.passport_secrets;

    const passportOpts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: SECRET,
        passReqToCallback: true,
    };

    passport.use(new JwtStrategy(passportOpts, function (req, jwtPayload, done) {
        const expirationDate = new Date(jwtPayload.exp * 1000);
        if (expirationDate < new Date()) {
            return done(null, false);
        }
        console.log(jwtPayload);

        done(null, jwtPayload);
    }))

    passport.serializeUser(function (user, done) {
        console.log(user);

        User.findOne({
            where: {
                userId: parseInt(user.userId),
            }
        }).then(userData => {
            const tempUser = {
                userId: userData.userId,
                mobileNo: user.username
            };
            done(null, tempUser);
        }).catch(err => {
            done(null, false);
        })
        // console.log('serializeUser', user); // <--- here pass userRid later
    });
}