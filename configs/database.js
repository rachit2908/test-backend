const { Sequelize } = require('sequelize');

module.exports = new Sequelize(process.env.database, process.env.user, process.env.password, {
    host: '18.140.32.212',
    dialect: 'mysql',
    // operatorsAliases: false,
    logging: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 1000
    },
    define: {
        timestamps: false
    }
});