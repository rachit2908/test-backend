const app = require('./app');
var http = require('http');
const port = process.env.PORT || 3000;

const server = http.createServer(app);


server.listen(port, function () {
    console.log("Express server listening on *:" + port);
});


// const https = require('https')
// var url = 'https://api.coinbase.com/v2/prices/spot?currency=USD';

// function makeCall (url) {
//     return new Promise((resolve, reject) => {
//         https.get(url,function (res) {
//             res.on('data', function (d) {
//                 resolve(JSON.parse(d));
//             });
//             res.on('error', function (e) {
//                 reject(e)
//             });
//         });
//     })

// }


// function handleResults(results){
//     return Number((results.data.amount))*2;
// }

// makeCall(url)
// .then(function(results){
//     console.log("test1 in makecall function ")
//     console.log(handleResults(results))
// })
// .catch(console.log)
