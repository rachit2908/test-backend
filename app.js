const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const router = express.Router();
require('dotenv').config()


// database 
const db = require('./configs/database')

const passport = require('passport');
// passport 
require('./configs/authentication')(passport);


db.authenticate().then((res) => console.log('db connected'))
    .catch((err) => console.log('error', err))
app.use(morgan('dev'));

app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
    next();
});

const userRoutes = require('./api/Routes/userRoutes');
const charityRoutes = require('./api/Routes/CharityRoutes');
const charityTransRoutes = require('./api/Routes/CharityTransactionsRoutes');
const finLedgerRoutes = require('./api/Routes/FinLedgerRoutes');
const finYearsRoutes = require('./api/Routes/FinYearsRoutes');


app.use('/user', userRoutes);
app.use('/charity', charityRoutes);
app.use('/trans', charityTransRoutes);
app.use('/ledger', finLedgerRoutes);
app.use('/finyear', finYearsRoutes);

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});


app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        status: -1,
        description: error.message,
        data: error
    })
});


module.exports = app;